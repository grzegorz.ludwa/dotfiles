#!/bin/bash

set -e

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

config_dir=${HOME}/.config
sudo chmod 775 $config_dir

ln -s ${script_dir}/nvim ${config_dir}/nvim
ln -s ${script_dir}/i3 ${config_dir}/i3
ln -s ${script_dir}/i3blocks ${config_dir}/i3blocks
ln -s ${script_dir}/terminator ${config_dir}/terminator
ln -s ${script_dir}/.pk10.zsh ~/.pk10.zsh
ln -s ${script_dir}/.zshrc ~/.zshrc

pictures_dir=$(xdg-user-dir PICTURES)
cp ${script_dir}/Pictures/* ${pictures_dir}
