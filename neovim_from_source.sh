# install prerequisites
sudo apt update
sudo apt-get install -yq \
	autoconf \
	automake \
	cmake \
	curl \
	doxygen \
	gettext \
	g++ \
	libtool \
	libtool-bin \
	ninja-build \
	pkg-config \
	ripgrep \
	unzip

git clone https://github.com/neovim/neovim --branch stable --depth 1

cd neovim
make CMAKE_BUILD_TYPE=RelWithDebInfo
sudo make install

sudo apt-get install -y python3-pynvim python3-venv python3-pip
sudo apt-get install -y default-jre

curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g \
	tree-sitter \
	tree-sitter-cli \
	neovim \
	fd-find

cd .. && rm -rf neovim
