#!/bin/bash

set -e

sudo apt-get update

sudo apt-get install -yq \
    i3 \
    i3blocks

# dependencies
sudo apt-get install -yq \
    acpi \
    alsa-tools \
    arandr \
    autorandr \
    blueman \
    bluetooth \
    clipit \
    curl \
    feh \
    fonts-font-awesome \
    gcc \
    git \
    libnotify-bin \
    locate \
    numlockx \
    pavucontrol \
    pcmanfm \
    policykit-1-gnome \
    policykit-desktop-privileges \
    pulseaudio-utils \
    redshift \
    ripgrep \
    rofi \
    shutter \
    terminator \
    zsh

python3 -m pip install --user \
    thefuck

# install fzf from sources, because the apt version is outdated
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# install i3lock-color (forked version of i3lock)
sudo apt install autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev
git clone https://github.com/Raymo111/i3lock-color.git /tmp/i3lock-color
(
    cd /tmp/i3lock-color
    git tag -f "git-$(git rev-parse --short HEAD)"
    ./build.sh
    ./install-i3lock-color.sh
)
