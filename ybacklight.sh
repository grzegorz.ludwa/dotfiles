set -e

INSTALL_FOLDER=~/.local/bin/

git clone https://github.com/szekelyszilv/ybacklight.git
cd ybacklight/src && mkdir build
gcc ybacklight.c -o ./build/ybacklight
mkdir -p $INSTALL_FOLDER
cp ./build/ybacklight $INSTALL_FOLDER
cd ../.. && rm -rf ybacklight
