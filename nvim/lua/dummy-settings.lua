local dummy_status_ok, dummy = pcall(require, 'dummy')
if not dummy_status_ok then
  return
end

dummy.setup {
  options = {
    icons_enabled = false,
    theme = 'gruvbox',
    component_separators = '|',
    section_separators = '',
  },
}
