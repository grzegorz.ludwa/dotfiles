-- NOTE:
-- I keep right now everything in on file, because it is easier to work on it at the begging
-- I will move it later to separate files again

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end
vim.opt.rtp:prepend(lazypath)

-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
-- This is also a good place to setup other settings (vim.opt)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Setup lazy.nvim
require("lazy").setup({
  spec = {
    -- IDE futures
    {
      'nvim-telescope/telescope.nvim', -- Fuzzy Finder (files, lsp, etc)
      dependencies = {
        { 'nvim-lua/plenary.nvim' },
        {
          "nvim-telescope/telescope-live-grep-args.nvim",
          -- This will not install any breaking changes.
          -- For major updates, this must be adjusted manually.
          version = "^1.0.0",
        },
      },
    },
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      build =
      'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
    },
    {
      "nvim-treesitter/nvim-treesitter-textobjects",    -- Additional textobjects for treesitter
      dependencies = "nvim-treesitter/nvim-treesitter", -- Highlight, edit, and navigate code
    },
    'numToStr/Comment.nvim',                            -- "gc" to comment visual regions/lines
    -- 'unblevable/quick-scope',                         -- For highlight unique characters in line
    'tpope/vim-sleuth',                                 -- Detect tabstop and shiftwidth automatically
    {
      "iamcco/markdown-preview.nvim",
      cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
      ft = { "markdown" },
      build = function() vim.fn["mkdp#util#install"]() end,
    },
    { -- File explorer
      "mikavilpas/yazi.nvim",
      event = "VeryLazy",
      keys = {
        -- 👇 in this section, choose your own keymappings!
        {
          -- NOTE: this requires a version of yazi that includes
          -- https://github.com/sxyazi/yazi/pull/1305 from 2024-07-18
          '<leader>0',
          "<cmd>Yazi toggle<cr>",
          desc = "Resume the last yazi session",
        },
        {
          "<leader>-",
          "<cmd>Yazi<cr>",
          desc = "Open yazi at the current file",
        },
        {
          -- Open in the current working directory
          "<leader>=",
          "<cmd>Yazi cwd<cr>",
          desc = "Open the file manager in nvim's working directory",
        },
      },
    },
    -- { -- File explorer
    --   'nvim-tree/nvim-tree.lua',
    --   dependencies = {
    --     'nvim-tree/nvim-web-devicons', -- optional, for file icons
    --   },
    -- },
    {
      'stevearc/conform.nvim',
      opts = {},
    },
    { 'akinsho/toggleterm.nvim', version = "*",                                     config = true },
    { 'thibthib18/ros-nvim',     dependencies = { 'nvim-telescope/telescope.nvim' } }, -- view ROS topics, msgs, srvs and so on
    {
      "CopilotC-Nvim/CopilotChat.nvim",
      branch = "canary",
      dependencies = {
        "zbirenbaum/copilot.lua", -- or github/copilot.vim
        "nvim-lua/plenary.nvim",  -- for curl, log wrapper
      },
    },
    -- Git
    'tpope/vim-fugitive',                                                      -- Git commands in nvim
    { 'lewis6991/gitsigns.nvim', dependencies = { 'nvim-lua/plenary.nvim' } }, -- Add git related info in the signs columns and popups
    -- Appearance
    'gruvbox-community/gruvbox',                                               -- Best themee ever!
    'nvim-lualine/lualine.nvim',                                               -- Fancier statusline
    'lukas-reineke/indent-blankline.nvim',                                     -- Add indentation guides even on blank lines
    -- LSP
    "williamboman/mason.nvim",                                                 -- Portable package manager for Neovim
    "williamboman/mason-lspconfig.nvim",
    {
      'mfussenegger/nvim-dap',               -- Debug Adapter Protocol client
      dependencies = {
        'theHamsta/nvim-dap-virtual-text',   -- Add virtual text support for DAP
        'nvim-telescope/telescope-dap.nvim', -- Telescope integration for DAP
        'mfussenegger/nvim-dap-python',      -- Python config for DAP
        'jay-babu/mason-nvim-dap.nvim',      -- bridges mason.nvim with the dap plugin
      },
    },
    {
      'rcarriga/nvim-dap-ui', -- A better UI for DAP
      dependencies = {
        'mfussenegger/nvim-dap',
        'nvim-neotest/nvim-nio',
      }
    },
    'neovim/nvim-lspconfig', -- Collection of configurations for built-in LSP client
    {
      'hrsh7th/nvim-cmp',    -- Autocompletion
      dependencies = {
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-path',
        'saadparwaiz1/cmp_luasnip',
        'hrsh7th/cmp-nvim-lua',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-cmdline',
      },
    },
    { 'L3MON4D3/LuaSnip',        dependencies = { 'saadparwaiz1/cmp_luasnip' } }, -- Snippet Engine and Snippet Expansion

    -- misc
    { -- Text areas in browser will neovim
      'glacambre/firenvim', build = ":call firenvim#install(0)",
    },
    {
      'https://gitlab.com/itaranto/plantuml.nvim',
      version = '*',
      config = function() require('plantuml').setup() end,
    },


    -- not used for now
    -- { 'dyng/ctrlsf.vim' }                                                       -- Search and display result in a user-friendly view with adjustable context
    -- 'tpope/vim-rhubarb'                                                         -- Fugitive-companion to interact with github
    -- 'mjlbach/onedark.nvim'                                                      -- Theme inspired by Atom
  },
  -- Configure any other settings here. See the documentation for more details.
  -- colorscheme that will be used when installing plugins.
  install = { colorscheme = { "habamax" } },
  -- automatically check for plugin updates
  checker = { enabled = true, notify = false },
})

-- [[ Configure settings ]] [[ begin ]]
vim.cmd [[colorscheme gruvbox]]             -- Set colorscheme

vim.o.hlsearch = false                      -- Set highlight on search
vim.o.mouse = 'a'                           -- Enable mouse mode
vim.o.cmdheight = 2                         -- More space for displaying messages
vim.o.breakindent = true                    -- Enable break indent
vim.o.termguicolors = true                  -- Enables 24-bit RGB color
vim.o.undofile = true                       -- Save undo history
vim.o.completeopt = 'menu,menuone,noselect' -- Set completeopt to have a better completion experience
vim.o.splitbelow = true                     -- when spliting horizontally then put at the bottom
vim.o.splitright = true                     -- when spliting vertically then put on the right
vim.o.showmode = false                      -- disable showing mode in the command line
vim.o.updatetime = 300                      -- Decrease update time
vim.o.timeoutlen = 500                      -- By default timeoutlen is 1000 ms
vim.o.scrolloff = 8                         -- Minimal number of screen lines to keep above and below the cursor

-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

vim.bo.swapfile = false      -- Disable swapfile creation
vim.wo.number = true         -- Make line numbers default
vim.wo.relativenumber = true -- Adds relative numbers on the left
vim.wo.signcolumn = 'yes'    -- Always add sign column
vim.wo.wrap = false          -- Display long lines as just one line
vim.wo.cursorline = true     -- Enable highlighting of the current line
vim.wo.list = true           -- List mode: By default, show tabs as ">", trailing spaces as "-", and non-breakable space characters as "+"
vim.opt.ruler = true         -- Show the cursor position all the time
-- [[ Configure settings ]] [[ end ]]

-- [[ Configure keymaps ]] [[ begin ]]
local function map(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  -- See `:help vim.keymap.set()`
  vim.keymap.set(mode, lhs, rhs, options)
end

map({ 'n', 'v' }, '<Space>', '<Nop>')

-- Reload config file without exiting neovim
map('n', '<leader>r', ':source %<CR>')

-- Remap for dealing with word wrap
map('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true })
map('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true })

-- Better indenting
map('v', '<', '<gv')
map('v', '>', '>gv')

-- Better window navigation
map('n', '<leader>h', ':wincmd h<CR>')
map('n', '<leader>j', ':wincmd j<CR>')
map('n', '<leader>k', ':wincmd k<CR>')
map('n', '<leader>l', ':wincmd l<CR>')

-- Paste under makred text
map('v', '<leader>p', '"_dP')

-- Copy into + buffer
map({ 'v', 'n' }, '<leader>y', '"+y')

map('t', '<Esc>', [[<C-\><C-n>]])
-- [[ Configure keymaps ]] [[ end ]]

-- [[ Auto commands settings ]] [[ begin ]]
-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- Auto set syntax for launch files
local syntax_group = vim.api.nvim_create_augroup('SyntaxSetGroup', { clear = true })
vim.api.nvim_create_autocmd({ 'BufRead', 'BufReadPost', 'BufNewFile' }, {
  pattern = '*.launch', command = 'set filetype=xml', group = syntax_group
})
-- -- Run black after save
-- vim.api.nvim_create_autocmd('BufWritePre', {
--   pattern = 'python', command = 'Black'
-- })

-- Auto set syntax for Jenkinsfile
-- local syntax_group = vim.api.nvim_create_augroup('SyntaxSetGroup', { clear = true })
-- vim.api.nvim_create_autocmd('BufReadPost', {
--   pattern = 'Jenkinsfile', command = 'syntax=groovy', group = syntax_group
-- })
-- [[ Auto commands settings ]] [[ end ]]

require('Comment').setup()

-- -- [[ Configure nvim-tree ]] [[ begin ]]
-- require('nvim-tree').setup {
--   actions = {
--     open_file = {
--       resize_window = false,
--     },
--   },
-- }
-- vim.keymap.set('n', '<leader>n', ':NvimTreeToggle<CR>', { noremap = true, silent = true })
-- vim.keymap.set('n', '<leader>nr', ':NvimTreeRefresh<CR>', { noremap = true, silent = true })
-- -- [[ Configure nvim-tree ]] [[ end ]]

-- [[ Configure conform ]] [[ begin ]]
require("conform").setup {
  formatters_by_ft = {
    lua = { "stylua" },
    -- Conform will run multiple formatters sequentially
    python = { "isort", "black" },
    -- You can customize some of the format options for the filetype (:help conform.format)
    rust = { "rustfmt", lsp_format = "fallback" },
    -- Conform will run the first available formatter
    javascript = { "prettierd", "prettier", stop_after_first = true },
  },
}
-- [[ Configure conform ]] [[ end ]]


-- [[ Configure ros-nvim ]] [[ begin ]]
local vim_utils = require "ros-nvim.vim-utils"
require 'ros-nvim'.setup {
  -- path to your catkin workspace
  catkin_ws_path = "/mower/mower_ws",

  -- make program (e.g. "catkin_make" or "catkin build" )
  catkin_program = "catkin build",

  --method for opening terminal for e.g. catkin_make: `vim_utils.open_new_buffer` or custom function
  open_terminal_method = function()
    vim_utils.open_split()
  end,

  -- terminal height for build / test, only valid with `open_terminal_method=open_split()`
  terminal_height = 8,

  -- Picker mappings
  node_picker_mappings = function(map)
    map("n", "<c-k>", vim_utils.open_terminal_with_format_cmd_entry("rosnode kill %s"))
    map("i", "<c-k>", vim_utils.open_terminal_with_format_cmd_entry("rosnode kill %s"))
  end,
  topic_picker_mappings = function(map)
    local cycle_previewers = function(prompt_bufnr)
      local picker = action_state.get_current_picker(prompt_bufnr)
      picker:cycle_previewers(1)
    end
    map("n", "<c-b>", vim_utils.open_terminal_with_format_cmd_entry("rostopic pub %s"))
    map("i", "<c-b>", vim_utils.open_terminal_with_format_cmd_entry("rostopic pub %s"))
    -- While browsing topics, press <c-e> to switch between `rostopic info` and `rostopic echo`
    map("n", "<c-e>", cycle_previewers)
    map("i", "<c-e>", cycle_previewers)
  end,
  service_picker_mappings = function(map)
    map("n", "<c-e>", vim_utils.open_terminal_with_format_cmd_entry("rosservice call %s"))
    map("i", "<c-e>", vim_utils.open_terminal_with_format_cmd_entry("rosservice call %s"))
  end,
  param_picker_mappings = function(map)
    map("n", "<c-e>", vim_utils.open_terminal_with_format_cmd_entry("rosparam set %s"))
    map("i", "<c-e>", vim_utils.open_terminal_with_format_cmd_entry("rosparam set %s"))
  end
}

vim.keymap.set('n', '<leader>rfp', require('ros-nvim.telescope.package').search_package,
  { desc = "[R]OS [F]ind files in current [P]ackage", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rfg', require('ros-nvim.telescope.package').grep_package,
  { desc = "[R]OS [F]ind [G]rep in current package", noremap = true, silent = true })

vim.keymap.set('n', '<leader>rtl', require('ros-nvim.telescope.pickers').topic_picker,
  { desc = "[R]OS [T]opics [L]ist", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rnl', require('ros-nvim.telescope.pickers').node_picker,
  { desc = "[R]OS [N]ode [L]ist", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rsl', require('ros-nvim.telescope.pickers').service_picker,
  { desc = "[R]OS [S]ervice [L]ist", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rpl', require('ros-nvim.telescope.pickers').param_picker,
  { desc = "[R]OS [P]aram [L]ist", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rsd', require('ros-nvim.telescope.pickers').srv_picker,
  { desc = "[R]OS [S]rv [D]efinitions", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rmd', require('ros-nvim.telescope.pickers').msg_picker,
  { desc = "[R]OS [M]sg [D]efinitions", noremap = true, silent = true })

vim.keymap.set('n', '<leader>rbw', require('ros-nvim.build').catkin_make,
  { desc = "[R]OS [B]uild [W]orkspace", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rbp', require('ros-nvim.build').catkin_make_pkg,
  { desc = "[R]OS [B]uild [P]ackage", noremap = true, silent = true })
vim.keymap.set('n', '<leader>rt', require('ros-nvim.test').rostest,
  { desc = "[R]OS [T]est", noremap = true, silent = true })
-- [[ Configure ros-nvim ]] [[ end ]]


-- [[ Configure copilot ]] [[ begin ]]
require('copilot').setup({
  panel = {
    enabled = true,
    auto_refresh = false,
    keymap = {
      jump_prev = "[[",
      jump_next = "]]",
      accept = "<CR>",
      refresh = "gr",
      open = "<M-CR>"
    },
    layout = {
      position = "bottom", -- | top | left | right
      ratio = 0.4
    },
  },
  suggestion = {
    enabled = true,
    auto_trigger = true,
    debounce = 75,
    keymap = {
      accept = false,
      accept_word = false,
      accept_line = false,
      next = "<M-]>",
      prev = "<M-[>",
      dismiss = "<C-]>",
    },
  },
  filetypes = {
    yaml = false,
    markdown = false,
    help = false,
    gitcommit = false,
    gitrebase = false,
    hgcommit = false,
    svn = false,
    cvs = false,
    ["."] = false,
  },
  copilot_node_command = 'node', -- Node.js version must be > 18.x
  server_opts_overrides = {},
})
-- [[ Configure copilot ]] [[ end ]]

-- [[ Configure CopilotChat ]] [[ begin ]]
local prompts = require('CopilotChat.prompts')
local select = require('CopilotChat.select')
require("CopilotChat").setup {
  system_prompt = prompts.COPILOT_INSTRUCTIONS, -- System prompt to use
  model = 'gpt-4',                              -- GPT model to use, 'gpt-3.5-turbo' or 'gpt-4'
  temperature = 0.1,                            -- GPT temperature
  context = 'manual',                           -- Context to use, 'buffers', 'buffer' or 'manual'
  proxy = nil,                                  -- [protocol://]host[:port] Use this proxy
  allow_insecure = false,                       -- Allow insecure server connections
  debug = false,                                -- Enable debug logging
  show_folds = true,                            -- Shows folds for sections in chat
  clear_chat_on_new_prompt = false,             -- Clears chat on every new prompt
  auto_follow_cursor = true,                    -- Auto-follow cursor in chat
  name = 'CopilotChat',                         -- Name to use in chat
  separator = '---',                            -- Separator to use in chat
  -- default prompts
  prompts = {
    Explain = {
      prompt = '/COPILOT_EXPLAIN Write an explanation for the code above as paragraphs of text.',
    },
    Tests = {
      prompt = '/COPILOT_TESTS Write a set of detailed unit test functions for the code above.',
    },
    Fix = {
      prompt = '/COPILOT_FIX There is a problem in this code. Rewrite the code to show it with the bug fixed.',
    },
    Optimize = {
      prompt = '/COPILOT_REFACTOR Optimize the selected code to improve performance and readablilty.',
    },
    Docs = {
      prompt = '/COPILOT_REFACTOR Write documentation for the selected code. The reply should be a codeblock containing the original code with the documentation added as comments. Use the most appropriate documentation style for the programming language used (e.g. JSDoc for JavaScript, docstrings for Python etc.',
    },
    FixDiagnostic = {
      prompt = 'Please assist with the following diagnostic issue in file:',
      selection = select.diagnostics,
    },
    Commit = {
      prompt = 'Write commit message for the change with commitizen convention. Make sure the title has maximum 50 characters and message is wrapped at 72 characters. Wrap the whole message in code block with language gitcommit.',
      selection = select.gitdiff,
    },
    CommitStaged = {
      prompt = 'Write commit message for the change with commitizen convention. Make sure the title has maximum 50 characters and message is wrapped at 72 characters. Wrap the whole message in code block with language gitcommit.',
      selection = function(source)
        return select.gitdiff(source, true)
      end,
    },
  },
  -- default selection (visual or line)
  selection = function(source)
    return select.visual(source) or select.line(source)
  end,
  -- default window options
  window = {
    layout = 'vertical',    -- 'vertical', 'horizontal', 'float'
    -- Options below only apply to floating windows
    relative = 'editor',    -- 'editor', 'win', 'cursor', 'mouse'
    border = 'single',      -- 'none', single', 'double', 'rounded', 'solid', 'shadow'
    width = 0.8,            -- fractional width of parent
    height = 0.6,           -- fractional height of parent
    row = nil,              -- row position of the window, default is centered
    col = nil,              -- column position of the window, default is centered
    title = 'Copilot Chat', -- title of chat window
    footer = nil,           -- footer of chat window
    zindex = 1,             -- determines if window is on top or below other floating windows
  },
  -- default mappings
  mappings = {
    complete = {
      detail = 'Use @<Tab> or /<Tab> for options.',
      insert = '<Tab>',
    },
    close = {
      normal = 'q',
      insert = '<C-c>'
    },
    reset = {
      normal = '<C-l>',
      insert = '<C-l>'
    },
    submit_prompt = {
      normal = '<CR>',
      insert = '<C-m>'
    },
    accept_diff = {
      normal = '<C-y>',
      insert = '<C-y>'
    },
    show_diff = {
      normal = 'gd'
    },
    show_system_prompt = {
      normal = 'gp'
    },
    show_user_selection = {
      normal = 'gs'
    },
  },
}
-- [[ Configure CopilotChat ]] [[ end ]]

-- [[ Configure Telescope ]] [[ begin ]]
-- See `:help telesceope` and `:help telescope.setup()`
local telescope_status_ok, telescope = pcall(require, 'telescope')
if not telescope_status_ok then
  return
end
local lga_actions = require("telescope-live-grep-args.actions")
local telescope_actions = require('telescope.actions')

telescope.setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
  pickers = {
    live_grep = {
      additional_args = function()
        return {
          "-u",
        }
      end
    },
    grep_string = {
      additional_args = function()
        return {
          "-u",
        }
      end
    },
    find_files = {
      hidden = true,
      no_ignore = true,
    },
  },
  extensions = {
    live_grep_args = {
      auto_quoting = true, -- enable/disable auto-quoting
      -- define mappings, e.g.
      mappings = {         -- extend mappings
        i = {
          ["<C-k>"] = lga_actions.quote_prompt(),
          ["<C-i>"] = lga_actions.quote_prompt({ postfix = " --iglob " }),
          -- freeze the current list and start a fuzzy search in the frozen list
          ["<C-space>"] = telescope_actions.to_fuzzy_refine,
        },
      },
      -- ... also accepts theme settings, for example:
      -- theme = "dropdown", -- use dropdown theme
      -- theme = { }, -- use own theme spec
      -- layout_config = { mirror=true }, -- mirror preview pane
    },
  },
}

-- Enable telescope fzf native, if installed
pcall(telescope.load_extension, 'fzf')
pcall(telescope.load_extension, 'live_grep_args')

-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
-- vim.keymap.set('n', '<leader>/', function()
--   -- You can pass additional configuration to telescope to change theme, layout, etc.
--   require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
--     winblend = 10,
--     previewer = false,
--   })
-- end, { desc = '[/] Fuzzily search in current buffer]' })

vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files, { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
-- vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sg', ":lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>",
  { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })
-- [[ Telescope settings ]] [[ end ]]


require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'gruvbox',
    component_separators = '|',
    section_separators = '',
    file_status = true, -- displays file status (readonly status, modified status)
    path = 2            -- 0 = just filename, 1 = relative path, 2 = absolute path
  },
}

-- require('ibl').setup()

-- [[ Configure quick-scope ]]
-- Trigger a highlight in the appropriate direction when pressing these keys:
vim.cmd([[
  let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
  highlight QuickScopePrimary guifg='#00C7DF' gui=underline ctermfg=155 cterm=underline
  highlight QuickScopeSecondary guifg='#afff5f' gui=underline ctermfg=81 cterm=underline
]])

-- [[ Configure DAP ]] [[ begin ]]
local debugpy_install_path = vim.fn.stdpath 'data' .. '/mason/packages/debugpy/venv/bin/python'
-- packages/debugpy/venv/bin/python
require('dap-python').setup(debugpy_install_path)

local dap = require('dap')
local cppdbg_install_path = vim.fn.stdpath 'data' .. '/mason/bin/OpenDebugAD7'
dap.adapters.cppdbg = {
  id = 'cppdbg',
  type = 'executable',
  command = cppdbg_install_path,
}

dap.configurations.cpp = {
  {
    name = "Launch file",
    type = "cppdbg",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopAtEntry = true,
  },
  {
    name = 'Attach to gdbserver :1234',
    type = 'cppdbg',
    request = 'launch',
    MIMode = 'gdb',
    miDebuggerServerAddress = 'localhost:1234',
    miDebuggerPath = '/usr/bin/gdb',
    cwd = '${workspaceFolder}',
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
  },
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp

local dap_ok, dap = pcall(require, "dap")

-- require("dapui").setup()

vim.fn.sign_define('DapBreakpoint', { text = '🐞' })

-- Start debugging session
vim.keymap.set("n", "<localleader>ds", function()
  dap.continue()
  ui.toggle({})
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-w>=", false, true, true), "n", false) -- Spaces buffers evenly
end)

-- Set breakpoints, get variable values, step into/out of functions, etc.
vim.keymap.set("n", "<localleader>dl", require("dap.ui.widgets").hover)
vim.keymap.set("n", "<localleader>dc", dap.continue)
vim.keymap.set("n", "<localleader>db", dap.toggle_breakpoint)
vim.keymap.set("n", "<localleader>dn", dap.step_over)
vim.keymap.set("n", "<localleader>di", dap.step_into)
vim.keymap.set("n", "<localleader>do", dap.step_out)
vim.keymap.set("n", "<localleader>dC", function()
  dap.clear_breakpoints()
end)

-- Close debugger and clear breakpoints
vim.keymap.set("n", "<localleader>de", function()
  dap.clear_breakpoints()
  ui.toggle({})
  dap.terminate()
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-w>=", false, true, true), "n", false)
end)
-- [[ Configure DAP ]] [[ end ]]

-- [[ Configure markdown preview ]]
vim.keymap.set({ "n", "i" }, '<F8>', '<Plug>MarkdownPreview', { silent = true })
vim.keymap.set({ "n", "i" }, '<F9>', '<Plug>MarkdownPreviewStop', { silent = true })

-- Gitsigns
-- See `:help gitsigns.txt`
require('gitsigns').setup {
  signs = {
    add = { text = '+' },
    change = { text = '~' },
    delete = { text = '_' },
    topdelete = { text = '‾' },
    changedelete = { text = '~' },
  },
}

-- [[ Configure Treesitter ]]
-- vim.opt.foldmethod     = 'expr'
-- vim.opt.foldexpr       = 'nvim_treesitter#foldexpr()'
---WORKAROUND
-- vim.api.nvim_create_autocmd({'BufEnter','BufAdd','BufNew','BufNewFile','BufWinEnter'}, {
--   group = vim.api.nvim_create_augroup('TS_FOLD_WORKAROUND', {}),
--   callback = function()
--     vim.opt.foldmethod     = 'expr'
--     vim.opt.foldexpr       = 'nvim_treesitter#foldexpr()'
--   end
-- })
---ENDWORKAROUND

-- See `:help nvim-treesitter`
require('nvim-treesitter.configs').setup {
  -- Add languages to be installed here that you want installed for treesitter
  ensure_installed = { 'c', 'cpp', 'cmake', 'dockerfile', 'gitignore', 'go', 'java', 'json', 'lua', 'make', 'markdown',
    'python', 'vim', 'vimdoc', 'yaml' },

  highlight = { enable = true },
  indent = { enable = true },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      -- TODO: I'm not sure for this one.
      scope_incremental = '<c-s>',
      node_decremental = '<c-backspace>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        -- ['af'] = '@function.outer',
        -- ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist)

-- LSP settings.
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we create a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]tion')

  nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
  nmap('gi', vim.lsp.buf.implementation, '[G]oto [I]mplementation')
  nmap('gr', require('telescope.builtin').lsp_references)
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<leader>D', vim.lsp.buf.type_definition, 'Type Definition')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function()
      require("conform").format({ async = true, lsp_format = "fallback" })
    end,
    { desc = 'Format current buffer with LSP' })
end

-- nvim-cmp supports additional completion capabilities
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

-- Enable the following language servers
local servers = { 'clangd', 'cmake', 'marksman', 'pyright', 'rust_analyzer', 'lua_ls', 'tsserver' }

require("mason").setup()
require("mason-nvim-dap").setup({
  ensure_installed = { 'cppdbg', 'python' }
})

-- Ensure the servers above are installed
require("mason-lspconfig").setup {
  ensure_installed = servers,
}

for _, lsp in ipairs(servers) do
  require('lspconfig')[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

-- Example custom configuration for lua
--
-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, 'lua/?.lua')
table.insert(runtime_path, 'lua/?/init.lua')

require('lspconfig').lua_ls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    lua = {
      runtime = {
        -- tell the language server which version of lua you're using (most likely luajit)
        version = 'luajit',
        -- setup your lua path
        path = runtime_path,
      },
      diagnostics = {
        globals = { 'vim' },
      },
      workspace = { library = vim.api.nvim_get_runtime_file('', true) },
    },
  },
}

require('lspconfig').pyright.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  before_init = function(params, config)
    local Path = require "plenary.path"
    local venv = Path:new((config.root_dir:gsub("/", Path.path.sep)), ".venv")
    if venv:joinpath("bin"):is_dir() then
      config.settings.python.pythonPath = tostring(venv:joinpath("bin", "python"))
    else
      config.settings.python.pythonPath = tostring(venv:joinpath("Scripts", "python.exe"))
    end
  end,
  settings = {
    python = {
      analysis = {
        autoSearchPaths = true,
        extraPaths = { '/opt/ros/noetic/lib/python3/dist-packages',
          '/mower/mower_ws/devel/lib/python3/dist-packages',
          '/usr/local/lib/python3.8/dist-packages',
          '/home/grzegorz/.local/lib/python3.8/site-packages' },
      }
    }
  },
}

require("lspconfig").clangd.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = {
    "clangd",
    "--offset-encoding=utf-16",
  },
}

-- nvim-cmp setup
local cmp = require('cmp')
local luasnip = require('luasnip')

cmp.setup({
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  window = {
    -- completion = cmp.config.window.bordered(),
    -- documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif require("copilot.suggestion").is_visible() then
        require("copilot.suggestion").accept()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = cmp.config.sources({
    { name = "nvim_lsp" },
    { name = "buffer" },
    { name = "path" },
    { name = "luasnip" },
    { name = "nvim_lua" },
  })
})

-- [[ Configure firenvim ]] [[ begin ]]
vim.g.firenvim_config = {
  localSettings = {
    [".*"] = {
      takeover = "never"
    }
  }
}
-- [[ Configure firenvim ]] [[ end ]]

require('plantuml').setup(
  {
    renderer = {
      type = 'image',
      options = {
        prog = 'eog',
        dark_mode = false,
        format = 'png', -- Allowed values: nil, 'png', 'svg'.
      }
    },
    render_on_write = true,
  }
)
