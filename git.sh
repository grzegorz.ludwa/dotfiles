#!/bin/bash

set -e

workdir="${HOME}/Downloads/git"
install_prefix="/usr"
git_tag="latest"

sudo apt-get update

# dev tools
sudo apt-get install -y curl wget jq

# remove git installed from apt
sudo apt-get remove -y git

# build tools
sudo apt-get install -y \
    dh-autoreconf \
    libcurl4-gnutls-dev \
    libexpat1-dev \
    gettext \
    libz-dev \
    libssl-dev

# for Debian-based distros
sudo apt-get install -y install-info

if [[ "${git_tag}" == "latest" ]]; then
    git_tag=$(curl --silent "https://api.github.com/repos/git/git/tags" | jq -r '.[0].name')
    if [[ -z "${git_tag}" ]]; then
        echo "Failed to retrieve the latest tag."
        exit 1
    fi
fi

echo 'Installing Git version' ${git_tag}
archive_name="git-${git_tag}.tar.gz"
extract_dir="git-${git_tag}"
mkdir -p "${workdir}"
cd "${workdir}"
mkdir -p "${extract_dir}"

wget https://github.com/git/git/archive/refs/tags/${git_tag}.tar.gz -O "${archive_name}"
tar xzf "${archive_name}" -C "${extract_dir}" --strip-components=1

cd "${extract_dir}"
make configure
./configure --prefix=${install_prefix}
make all
sudo make install

rm -rf ${workdir}/git-2.39.1
rm -f ${workdir}/git-2.39.1.tar.gz
