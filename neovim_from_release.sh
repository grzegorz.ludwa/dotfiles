# download latest stable release from github and put into ~/.local
wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.tar.gz
tar xzvf nvim-linux64.tar.gz
mkdir -p ~/.local
cp -r nvim-linux64/* ~/.local/
rm -rf nvim-linux64 nvim-linux64.tar.gz

# install prerequisites
sudo apt-get update
sudo apt-get install -y python3-pynvim python3-venv python3-pip
sudo apt-get install -y default-jre

# install nodejs
sudo apt-get install -y ca-certificates curl gnupg
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=20
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
sudo apt-get update
sudo apt-get install -y nodejs

# install nodejs packages
sudo npm --force install -g \
    tree-sitter \
    tree-sitter-cli \
    neovim \
    fd-find
