# monitor hotplug

On i3 there is no monitor hotplug switching mechanism, but it is easy to create on your own.

## How to

1. Copy `change_display.sh` script to your system.
2. Make sure it can be executed: `chmod +x change_display.sh`.
3. Open `change_display.sh` and modify:
  1. `USER` - should be the name of your user (because udev rule will execute this script as root)
  2. `DISPLAY` - run `echo $DISPLAY` while logged into X-window session and put printed value into variable
  3. Modify `xrandr` commands to match your needs (I left mine as example).
4. Copy `99-monitor-hotplug.rules` into `/etc/udev/rules.d/`.
5. Modify path of `change_display.sh` script in `/etc/udev/rules.d/99-monitor-hotplug.rules`.
6. Run `sudo udevadm control --reload-rules && udevadm trigger`.
