#!/bin/bash

# Adapt it to your hdmi or output card
USER=<fill in your user name>
DISPLAY=<fill in your display>
HDMI_CONNECTED=`cat /sys/class/drm/card1-HDMI-A-1/status | grep -Ec "^connected"`

if [ "$HDMI_CONNECTED" -eq 1 ]; then
  /bin/sleep 2s;
  su -c "DISPLAY=$DISPLAY /usr/bin/xrandr --output eDP-1 --auto --output HDMI-1-0 --right-of eDP-1 --auto --primary" $USER
else
  /bin/sleep 2s;
  su -c "DISPLAY=$DISPLAY /usr/bin/xrandr --output eDP-1 --auto --primary --output HDMI-1-0 --off" $USER
fi
